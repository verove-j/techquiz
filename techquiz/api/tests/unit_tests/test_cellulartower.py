import pytest
from pytest import raises
from django.urls import reverse
from api.core.cellulartower import filter_cellulartower_from_db
import json
from unittest.mock import patch
from api.models.cellulartower import CellularTower


## TEST MODEL CELLULARTOWER
@pytest.fixture(scope="module")
def new_cellulartower():
    cellular_tower = CellularTower(
        radio="GSM",
        mcc=270,
        net=10,
        area=12,
        cell=1,
        unit=1,
        lon="1.0000000000",
        lat="1.0000000000",
        range=1,
        samples=1,
        changeable=1,
        created=1,
        updated=1,
        averageSignal=1
    )
    return cellular_tower

def test_new_cellulartower(new_cellulartower):
    assert new_cellulartower.radio == "GSM"
    assert new_cellulartower.mcc == 270
    assert new_cellulartower.net == 10
    assert new_cellulartower.area == 12
    assert new_cellulartower.cell == 1
    assert new_cellulartower.unit == 1
    assert new_cellulartower.lon == "1.0000000000"
    assert new_cellulartower.lat == "1.0000000000"
    assert new_cellulartower.range == 1
    assert new_cellulartower.samples == 1
    assert new_cellulartower.changeable == 1
    assert new_cellulartower.created == 1
    assert new_cellulartower.updated == 1
    assert new_cellulartower.averageSignal == 1



## TEST DATABASE SEARCH FUNCTION FOR CELLULARTOWER LIST
class TestCellularTower:

    @patch('api.core.cellulartower.CellularTower')
    def test_cellulartower_params(self, cellulartower_mock):
        params = {'mcc': 1, 'net': 1, 'area': 1, 'cell': 1}
        response = filter_cellulartower_from_db(cellulartower_mock.objects, params)
        cellulartower_mock.objects.filter.assert_called_with(**params)
        cellulartower_mock.objects.filter.assert_called_once()
        assert isinstance(response, type(cellulartower_mock.objects.filter()))

    @patch('api.core.cellulartower.CellularTower')
    def test_cellulartower_param_none(self, cellulartower_mock):
        params = None
        with raises(Exception):
            filter_cellulartower_from_db(params)
    
    @patch('api.core.cellulartower.CellularTower')
    def test_cellulartower_without_data(self, cellulartower_mock):
        params = {'mcc': 1, 'net': 1, 'area': 1, 'cell': 1}
        cellulartower_mock.objects.filter.side_effect = Exception()
        with raises(Exception):
            filter_cellulartower_from_db(params)

from django.test import TestCase

from api.models.cellulartower import CellularTower

import json

cellularTowersMock = {
    1: {"radio": "GSM", "mcc": 270, "net": 10, "area": 12, "cell": 1, "unit": 1, "lon": "1.0000000000", "lat": "1.0000000000", "range": 1, "samples": 1, "changeable": 1, "created": 1, "updated": 1, "averageSignal": 1},
    2: {"radio": "GSM", "mcc": 270, "net": 77, "area": 12, "cell": 2, "unit": 1, "lon": "1.0000000000", "lat": "1.0000000000", "range": 1, "samples": 1, "changeable": 1, "created": 1, "updated": 1, "averageSignal": 1},
    3: {"radio": "GSM", "mcc": 270, "net": 10, "area": 1, "cell": 3, "unit": 1, "lon": "1.0000000000", "lat": "1.0000000000", "range": 1, "samples": 1, "changeable": 1, "created": 1, "updated": 1, "averageSignal": 1},
    4: {"radio": "GSM", "mcc": 270, "net": 10, "area": 12, "cell": 4, "unit": 1, "lon": "1.0000000000", "lat": "1.0000000000", "range": 1, "samples": 1, "changeable": 1, "created": 1, "updated": 1, "averageSignal": 1}
}

class CellularTowerViewSetTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        for cellularTowerMock in cellularTowersMock.items():
            CellularTower.objects.create(
                radio = cellularTowerMock[1]["radio"],
                mcc = cellularTowerMock[1]["mcc"],
                net = cellularTowerMock[1]["net"],
                area = cellularTowerMock[1]["area"],
                cell = cellularTowerMock[1]["cell"],
                unit = cellularTowerMock[1]["unit"],
                lon = cellularTowerMock[1]["lon"],
                lat = cellularTowerMock[1]["lat"],
                range = cellularTowerMock[1]["range"],
                samples = cellularTowerMock[1]["samples"],
                changeable = cellularTowerMock[1]["changeable"],
                created = cellularTowerMock[1]["created"],
                updated = cellularTowerMock[1]["updated"],
                averageSignal = cellularTowerMock[1]["averageSignal"]
            )

    def test_view_url_without_parameter(self):
        response = self.client.get('/cellulartower/')
        self.assertEqual(response.status_code, 400)

    def test_cellular_tower_list_cell(self):
        """
        Test the GET request cellular tower list with one parameter (cell).
        """
        cell_value = 1
        response = self.client.get('/cellulartower/', {'cell': cell_value})
        response_content = json.loads(response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response_content['results'],
            [cellularTowerMock[1] 
            for cellularTowerMock in cellularTowersMock.items() 
            if cellularTowerMock[1]['cell'] == cell_value]
        )

    def test_cellular_tower_list_mcc_net(self):
        """
        Test the GET request cellular tower list with 2 parameters (mcc and net).
        """
        mcc_value = 270
        net_value = 10
        response = self.client.get('/cellulartower/', {'mcc': mcc_value, 'net': net_value})
        response_content = json.loads(response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response_content['results'],
            [cellularTowerMock[1] 
            for cellularTowerMock in cellularTowersMock.items() 
            if cellularTowerMock[1]['mcc'] == mcc_value 
            and cellularTowerMock[1]['net'] == net_value]
        )

    def test_cellular_tower_list_mcc_net_area(self):
        """
        Test the GET request cellular tower list with <mcc> parameter.
        """
        mcc_value = 270
        net_value = 10
        area_value = 12
        response = self.client.get('/cellulartower/', {'mcc': mcc_value, 'net': net_value, 'area': area_value})
        response_content = json.loads(response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response_content['results'],
            [cellularTowerMock[1] 
            for cellularTowerMock in cellularTowersMock.items() 
            if cellularTowerMock[1]['mcc'] == mcc_value 
            and cellularTowerMock[1]['net'] == net_value
            and cellularTowerMock[1]['area'] == area_value]
        )
from rest_framework import viewsets, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.decorators import action
from api.models.cellulartower import CellularTower
from api.serializers.cellulartower import CellularTowerSerializer
from api.core.cellulartower import filter_cellulartower_from_db
from api.pagination.common_pagination import CommonPagination


class CellularTowerViewSet(viewsets.ModelViewSet):
    queryset = CellularTower.objects.all()
    serializer_class = CellularTowerSerializer
    pagination_class = CommonPagination

    def list(self, request):
        """
        List cellular tower.
        GET method will send the list of cellular tower corresponding to <mcc>, <net>, <area> and <cell> parameters value.
        If no parameter is provided, a list of every cellular tower will be send as response.
        """
        try:
            requested_cellular_towers = filter_cellulartower_from_db(self.get_queryset().order_by('cell'), request.GET)
            page = self.paginate_queryset(requested_cellular_towers)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
            else:
                serializer = self.get_serializer(requested_cellular_towers, many=True)

            result = self.get_paginated_response(serializer.data)
            data = result.data
            return Response(data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(
                {'message': str(e)},
                status=status.HTTP_400_BAD_REQUEST
            )
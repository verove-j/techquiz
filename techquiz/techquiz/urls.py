from django.urls import include, path
from django.conf.urls import url
from rest_framework import routers
from api.views.cellulartower import CellularTowerViewSet

router = routers.DefaultRouter()
router.register(r'cellulartower', CellularTowerViewSet, basename='cellulartower')
urlpatterns = [
    url(r'^', include(router.urls)),
    #path('cellulartower', CellularTowerViewSet.cellular_tower_list, name="cellulartower"),
]
urlpatterns += router.urls





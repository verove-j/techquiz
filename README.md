# Techquizz

Techquizz is a Django api communicating with a MySQL database. MySQL database is populated by a python script (scraper). Both Django api, MySQL DB and python scraper are containerized with Docker.

## Usage

Use the docker-compose command to build the MySQL database container, the Django api container and the scraper container
```console
foo@bar:~/project_root$sudo docker-compose up
```

## Running unit tests locally

Use the pytest command to run unit tests
```console
foo@bar:~/project_root/techquiz$pytest -vv
```

## Running functional tests locally

Use django test via django manage script to run functional tests
```console
foo@bar:~/project_root/techquiz$service mysql start #start the service mysql
foo@bar:~/project_root/techquiz$DB_USER=mysql_user DB_PASSWORD=mysql_password DB_HOST=127.0.0.1 DB_PORT=3306 DB_NAME=opencellid ./manage.py test api.tests.functional_tests.test_cellulartower #run django test with proper env var
```

## Manage the database from a GUI

- Go to your web browser and look for url "http://localhost:8080"
- Login to your database
- enjoy
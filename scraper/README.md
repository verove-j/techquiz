# Scraper

This Python3.8 script read a CSV file and populate a Mysql database.

## Usage

```console
foo@bar:~$source /MY/VENV/PATH/bin/activate  # run your virtual environement
foo@bar:~$python scraper.py -i data_samples/test.csv  # run the script by passing a csv file as argument
```
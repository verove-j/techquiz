#!/usr/bin/env python
import sys, os
import argparse
import pandas as pd
from models.cellulartower import CellularTower
from DB.db_connect import session, db_table
from DB.config import MYSQL_DATABASE


db_columns = [
    'radio',
    'mcc',
    'net',
    'area',
    'cell',
    'unit',
    'lon',
    'lat',
    'range',
    'samples',
    'changeable',
    'created',
    'updated',
    'averageSignal'
]

def populate_db_from_csv(input_file):
    chunksize = 1

    print('Populating MySQL database...')
    for df in pd.read_csv(input_file, chunksize=chunksize, usecols=db_columns, sep=','):
        fields_from_csv = df.to_dict(orient='records')
        try :
            fields = fields_from_csv[0]
            new_obj = CellularTower(**fields)
            session.merge(new_obj)
        except Exception as e:
            print(str(e))
    try :
        session.commit()
    except Exception as e:
        print(str(e))
        
    session.close()
    print('The table ', db_table, ' on MySQL database ', MYSQL_DATABASE, ' is populated. You can check your db.')    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parse a CSV file and populate a MySQL database in an idempotent way !')
    parser.add_argument(
        '-i',
        help='input file path',
        type=str,
        required=True,
        )
    args = parser.parse_args()
    populate_db_from_csv(args.i)